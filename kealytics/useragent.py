import re
from typing import Dict
from typing import Tuple

import confuse

from ua_parser import user_agent_parser


class UserAgentParser:
    _apps: Dict[str, re.Pattern]

    def __init__(self, apps: Dict[str, str]):
        """Create a new UserAgentParser."""
        self._apps = {
            r: re.compile(apps[r])
            for r in apps
        }

    def match_app(self, ua_string: str) -> str:
        """Return the name for an app given a user agent string.

        This makes use of the regular expression mappings given when the
        object was constructed.

        :param ua_string: user agent string to match on
        """
        for appname, appregex in self._apps.items():
            if appregex.match(ua_string.lower()):
                return appname
        return "other"

    def parse_ua(self, ua_string: str) -> Tuple[str, str]:
        """Return the name of an app and its platform given a user agent string.

        This makes use of the regular expression mappings given when the
        object was constructed. The platform will be determined by an educated
        guess and magic.

        :param ua_string: user agent string to match on
        """
        parsed_string = user_agent_parser.Parse(ua_string)
        platform = parsed_string['os']['family'].lower()
        appname = self.match_app(ua_string)
        return appname, platform

    @classmethod
    def from_confuse(cls, sv: confuse.Subview):  # -> UserAgentParser
        """
        Create a UserAgentParser from a confuse subview.

        .. note::

           Some caveats may apply for the use of backslash characters inside
           strings defining regular expressions. These should be investigated.

        :rtype: UserAgentParser
        """
        apps = sv.get(dict)
        return cls(apps)
