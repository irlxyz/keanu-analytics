import enum
from typing import Dict
from typing import Optional

import confuse

from flask import Flask, jsonify, request

import geoip2.database

import nio

import psycopg2

from kealytics.geo import GeoBuckets
from kealytics.homeserver import ConnectionInfo
from kealytics.homeserver import Homeserver
from kealytics.homeserver import event_is_message
from kealytics.homeserver import most_recent_connection
from kealytics.period import period_for_timestamp
from kealytics.useragent import UserAgentParser

app = Flask(__name__)


class EventType(enum.IntEnum):
    USER_GROUP_MSG = 1
    USER_DIRECT_MSG = 2
    GROUP_MSG = 3


class KeanuAnalytics:  # pragma: no cover
    _conn: psycopg2.extensions.connection
    homeserver: Homeserver
    geo_db: geoip2.database.Reader
    geo_buckets: GeoBuckets
    uap: UserAgentParser

    def __init__(self, config):
        self.homeserver = Homeserver(
            config["synapse"]["homeserver_fqdn"].get(str),
            config["synapse"]["as_token"].get(str))
        self.geo_db = geoip2.database.Reader(
            config["geo_regions"]["database"].as_filename())
        self.geo_buckets = GeoBuckets.from_confuse(
            config["geo_regions"]["buckets"])
        self.uap = UserAgentParser.from_confuse(
            config["user_agents"])
        dbargs: Dict[str, str] = {}
        for dbkey in ["dbname", "user", "password", "host", "port"]:
            v: str = config["database"][dbkey].get(str)
            if v != "default_sentinel":
                dbargs[dbkey] = v
        self._conn = psycopg2.connect(**dbargs)

    def handle_event(self, event: nio.Event) -> None:
        if not event_is_message(event):
            return
        if not self.homeserver.event_is_local(event):
            return
        try:
            period = period_for_timestamp(event.server_timestamp)
        except AttributeError:
            # MegolmEvents do not have origin_server_ts
            # TODO: Decrypt them
            period = period_for_timestamp(None)
        self.handle_message_event(period, event.sender,
                                  event.source["room_id"])

    def handle_message_event(self, period: str, sender: str,
                             room_id: str) -> None:
        joined_room_members = self.homeserver.get_room_members(room_id, sender)
        event_type: Optional[EventType] = None
        try:
            if len(joined_room_members.members) == 2:
                event_type = EventType.USER_DIRECT_MSG
            elif len(joined_room_members.members) > 2:
                event_type = EventType.USER_GROUP_MSG
        except AttributeError:
            return  # Could not resolve room, so we have no event type
        if event_type is None:
            return  # Room had < 2 members
        if event_type == EventType.USER_GROUP_MSG:
            if not self.is_event_counted(period, room_id):
                self.record_event(period, room_id, "", "", "",
                                  EventType.GROUP_MSG, True, False)
        is_first_period = not self.is_event_counted(period, sender)
        is_first_ever = not self.is_event_counted(None, sender)
        user_conn: ConnectionInfo = most_recent_connection(
            self.homeserver.get_whois(sender))
        try:
            city = self.geo_db.city(user_conn.ip)
            if city.location.latitude is None or city.location.longitude is None:
                bucket = "other"
            else:
                bucket = self.geo_buckets.bucket(city.location.latitude,
                                                 city.location.longitude)
        except geoip2.errors.AddressNotFoundError:
            bucket = "other"
        appname, platform = self.uap.parse_ua(user_conn.user_agent)
        self.record_event(period, sender, bucket, appname, platform,
                          event_type, is_first_period, is_first_ever)

    def is_event_counted(self, period: Optional[str], user: str) -> bool:
        query = ("SELECT COUNT(*) FROM analytics.period_engagement WHERE "
                 "user_id = %s")
        data = [user]
        if period is not None:
            query += " AND period = %s"
            data.append(period)
        with self._conn:
            with self._conn.cursor() as cur:
                cur.execute(query, data)
                result = cur.fetchone()
                return True if result[0] != 0 else False

    def record_event(self, period: str, user: str, bucket: str, appname: str,
                     platform: str, event: EventType,
                     is_first_period: bool, is_first_ever: bool) -> None:
        with self._conn:
            with self._conn.cursor() as cur:
                if is_first_period:
                    cur.execute("INSERT INTO analytics.period_engagement "
                                "(period, user_id) "
                                "VALUES (%s, %s)",
                                (period, user))
                cur.execute("INSERT INTO analytics.events "
                            "(period, bucket, appname, platform, "
                            "event_type, is_first_period, is_first_ever) "
                            "VALUES (%s, %s, %s, %s, %s, %s, %s)",
                            (period, bucket, appname, platform, event.value,
                             is_first_period, is_first_ever))


@app.route("/transactions/<transaction>", methods=["PUT"])
def on_receive_events(transaction):
    """Handle incoming transactions from the homeserver."""
    if request.args["access_token"] != config["synapse"]["hs_token"].get(str):
        print("bad access token")
        return jsonify({"error": "incorrect token from synapse"})
    events = request.get_json()["events"]
    for event in events:
        analytics.handle_event(nio.Event.parse_event(event))
    return jsonify({})


def run_server():  # pragma: no cover
    """Run as an application service."""
    global config
    config = confuse.Configuration('KeanuAnalytics', 'kealytics')
    global analytics
    analytics = KeanuAnalytics(config)
    app.run()


if __name__ == "__main__":
    run_server()
