import urllib.parse
from dataclasses import dataclass
from typing import Any
from typing import Dict
from typing import List
from typing import Union

import nio

import requests

KeanuMessageEvent = Union[
    nio.events.room_events.MegolmEvent,
    nio.events.room_events.RoomMessageText
]


@dataclass
class ConnectionInfo:
    """Information about a user connection to the homeserver."""

    ip: str
    """IP address of the user."""
    last_seen: int
    """Unix timestamp for the last user activity."""
    user_agent: str
    """User agent's self-identification string."""


@dataclass
class SessionInfo:
    """Information about a user session.

    A session includes all connections using the access token from a single
    login.
    """

    connections: List[ConnectionInfo]
    """Connections belonging to the session."""

    @classmethod
    def from_dict(cls, parsed_dict: Dict[Any, Any]):  # -> SessionInfo
        """Create a new SessionInfo using a parsed dictionary.

        :param parsed_dict: parsed JSON dictionary from an admin whois request.
        """
        connections = []
        for connection in parsed_dict["connections"]:
            connections.append(ConnectionInfo(
                connection["ip"],
                connection["last_seen"],
                connection["user_agent"]
            ))
        return cls(connections)


@dataclass
class DeviceInfo:
    """Information about a user device."""

    sessions: List[SessionInfo]
    """Sessions belonging to the device."""

    @classmethod
    def from_dict(cls, parsed_dict: Dict[Any, Any]):
        """Create a new DeviceInfo using a parsed dictionary.

        :param parsed_dict: parsed JSON dictionary from an admin whois request.
        """
        sessions = []
        for session in parsed_dict["sessions"]:
            sessions.append(SessionInfo.from_dict(session))
        return cls(sessions)


@dataclass
class AdminWhoisResponse:
    user_id: str
    """Matrix user identifier."""
    devices: Dict[str, DeviceInfo]
    """Devices belonging to the user."""

    @classmethod
    def from_dict(cls, parsed_dict: Dict[Any, Any]):
        """Create a new AdminWhoisResponse using a parsed dictionary.

        :param parsed_dict: parsed JSON dictionary from an admin whois request.
        """
        devices = dict()
        for device in parsed_dict["devices"]:
            devices[device] = DeviceInfo.from_dict(
                parsed_dict["devices"][device]
            )
        return cls(parsed_dict["user_id"], devices)


def most_recent_connection(whois: AdminWhoisResponse) -> ConnectionInfo:
    """Return the most recently seen connection information.

    .. note:: The last seen timestamps seem to be updated regularly
              but not immediately. If you have two connections active at once
              you can't tell for sure which device was sending the message at
              the time (at least via the API methods used here).
    """
    connections = []
    for device in whois.devices:
        for session in whois.devices[device].sessions:
            for connection in session.connections:
                connections.append(connection)
    if connections:
        connections.sort(reverse=True, key=lambda x: x.last_seen)
        return connections[0]
    return ConnectionInfo("127.0.0.100", 0, "Dummy/1.0")


def event_is_message(event: nio.Event) -> bool:
    """Return True if a Matrix event contains a message.

    :param event: The event to test.
    """
    if isinstance(event, nio.events.room_events.MegolmEvent):
        return True
    if isinstance(event, nio.events.room_events.RoomMessageText):
        return True
    return False


class Homeserver:  # pragma: no cover
    _api: nio.Api
    _fqdn: str
    _token: str

    def __init__(self, fqdn: str, token: str):
        self._api = nio.Api()
        self._fqdn = fqdn
        self._token = token

    def _make_cs_request(self, method: str, path: str,
                         user: str) -> Dict[Any, Any]:
        """Make a request to the homeserver via the client-server API.

        API requests can be built using the matrix-nio library. The path is
        modified by this function to include the user ID that the application
        service should impersonate. The decoded JSON response is returned.
        """
        if method != "GET":
            raise NotImplementedError
        urlp = urllib.parse.urlparse(path)
        qd = urllib.parse.parse_qs(urlp.query)
        qd["user_id"] = [user]
        qs = urllib.parse.urlencode(qd, doseq=True)
        urlp = urlp._replace(scheme="https", netloc=self._fqdn, query=qs)
        url = urllib.parse.urlunparse(urlp)
        r = requests.get(url)
        return r.json()

    def get_whois(self, user: str) -> AdminWhoisResponse:
        """Return method and URL for an admin whois API call.

        This call is not implemented in matrix-nio.
        """
        method = "GET"
        path = "/_matrix/client/r0/admin/whois/" + urllib.parse.quote(user)
        path += "?access_token=" + self._token
        return AdminWhoisResponse.from_dict(
            self._make_cs_request(method, path, user))

    def get_room_members(self, room,
                         user) -> nio.responses.JoinedMembersResponse:
        c = self._api.joined_members(self._token, room)
        return nio.responses.JoinedMembersResponse.from_dict(
            self._make_cs_request(c[0], c[1], user), room)

    def event_is_local(self, event: nio.Event) -> bool:
        return event.sender.endswith(self._fqdn)
