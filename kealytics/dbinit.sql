-- Initialise PostgreSQL database for KeanuAnalytics

CREATE SCHEMA IF NOT EXISTS analytics;

CREATE TABLE IF NOT EXISTS analytics.events (
  period                     TEXT,
  bucket                     TEXT,
  appname                    TEXT,
  platform                   TEXT,
  event_type                 INTEGER,
  is_first_period            BOOLEAN,
  is_first_ever              BOOLEAN
);

CREATE TABLE IF NOT EXISTS analytics.period_engagement (
  period                     TEXT,
  user_id                    TEXT
);

CREATE UNIQUE INDEX IF NOT EXISTS period_engagement_idx
ON analytics.period_engagement (
    period, user_id
);

CREATE INDEX IF NOT EXISTS forever_engagement_idx
ON analytics.period_engagement (
    user_id
);
