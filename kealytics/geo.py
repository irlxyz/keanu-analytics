from __future__ import annotations

"""Place geographic locations into buckets.

Classes:

    GeoBuckets

Functions:

    read_geojson(str) -> shapely.geometry.base.BaseGeometry

"""

import json
from collections import OrderedDict
from typing import Dict
from typing import List

import confuse

from shapely.geometry import GeometryCollection
from shapely.geometry import Point
from shapely.geometry import shape
from shapely.geometry.base import BaseGeometry


def read_geojson(path: str) -> BaseGeometry:
    """Read a `GeoJSON <https://geojson.org/>`_ file.

    The file is read from a given path and will have all features merged into a
    `Shapely <https://shapely.readthedocs.io/>`_ geometry.

    :param path: Filesystem path to GeoJSON file
    """
    with open(path) as input_file:
        features = json.load(input_file)["features"]
        result = None
        for geom in GeometryCollection(
                [shape(feature["geometry"]).buffer(0)
                 for feature in features]):
            result = geom if not result else result | geom
        return result


class GeoBuckets:
    """Geographic region buckets for discrete categorization of locations.

    Region configuration must be passed as a list of dictionaries, with each
    dictionary having a "name" and "path" key. For example:

    >>> from kealytics.geo import GeoBuckets
    >>> config = [
    ...   {"name": "ma", "path": "../examples/ma.geojson"},
    ...   {"name": "usa", "path": "../examples/usa.geojson"},
    ... ]
    >>> gb = GeoBuckets(config)
    >>> gb
    <GeoBuckets ma,usa>

    The order of the configuration elements matters. If more than one geometry
    would contain a point, the first entry in the list will be the name that
    is returned.

    :param bucket_config: Geographic region configuration
    """

    _buckets: OrderedDict[str, BaseGeometry]

    def __init__(self, bucket_config: List[Dict[str, str]]):
        """Construct new geobuckets from configuration."""
        self._buckets = OrderedDict()
        for bucket in bucket_config:
            self._buckets[bucket["name"]] = read_geojson(bucket["path"])

    def bucket(self, lat: float, lon: float) -> str:
        """Place a point into a bucket and return that bucket name.

        If no region matches, "other" is returned.

        >>> gb.bucket(42.360092, -71.094162) # MIT, Massachusetts
        'ma'
        >>> gb.bucket(38.897675, -77.036530) # White House, Washington
        'usa'
        >>> gb.bucket(45.421532, -75.697189) # Ottawa, Canada
        'other'

        :param lat: Decimal latitude
        :param lon: Decimal longitude
        """
        point = Point(lon, lat)
        for name in self._buckets:
            if point.within(self._buckets[name]):
                return name
        return "other"

    @classmethod
    def from_confuse(cls, sv: confuse.Subview):  # -> GeoBuckets
        """Create a GeoBuckets instance from a confuse subview.

        This has the advantage that confuse will provide an absolute path
        for files relative to the configuration directory.

        :rtype: GeoBuckets
        """
        parsed_config = []
        bucket_count = len(sv.get())
        for bucket_index in range(0, bucket_count):
            parsed_config.append(
                {"name": sv[bucket_index]["name"].get(str),
                 "path": sv[bucket_index]["path"].as_filename()})
        return cls(parsed_config)

    def __repr__(self) -> str:
        """Return representation of self."""
        return "<GeoBuckets " + ",".join(self._buckets.keys()) + ">"
