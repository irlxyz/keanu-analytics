-- period: the week being reported on
-- date: the first day in the period
-- appname: the normalized name of the user's client
-- platform: the normalized name of the user's platform
-- bucket: the geo region that the user's most recently seen ip address was geolocated into
-- engaged_users_new: the number of users who engaged this period
-- engaged_users_period: the number of users who were engaged for the first time ever this period

SELECT
e1.period,
TO_CHAR(TO_DATE(e1.period, 'IYYY"-W"IW'), 'YYYY-MM-DD') AS date,
e1.bucket,
e1.appname,
e1.platform,
(SELECT COUNT(*) FROM analytics.events AS e2 WHERE
  e1.period = e2.period AND
  e1.bucket = e2.bucket AND
  e1.appname = e2.appname AND
  e1.platform = e2.platform AND
  (e2.event_type = 1 OR e2.event_type = 2) AND
  e2.is_first_ever = TRUE
 GROUP BY e2.period, e2.bucket, e2.appname, e2.platform) AS engaged_users_new,
(SELECT COUNT(*) FROM analytics.events AS e2 WHERE
  e1.period = e2.period AND
  e1.bucket = e2.bucket AND
  e1.appname = e2.appname AND
  e1.platform = e2.platform AND
  (e2.event_type = 1 OR e2.event_type = 2) AND
  e2.is_first_period = TRUE
 GROUP BY e2.period, e2.bucket, e2.appname, e2.platform) AS engaged_users_period
FROM analytics.events AS e1
WHERE (e1.event_type = 1 OR e1.event_type = 2)
GROUP BY e1.period, e1.bucket, e1.appname, e1.platform
