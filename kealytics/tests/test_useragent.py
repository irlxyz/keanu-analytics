from nose.tools import assert_equal

from kealytics.useragent import UserAgentParser

def check_useragent(ua_string: str, expected_appname: str,
                    expected_platform: str) -> None:
    apps = {
        "keanu": r"^keanu_example/.*",
        "riot-desktop": r"^mozilla/5\.0.+riot/[0-9].+electron/[0-9].*$",
        "riot.im": r"^riot\.im/.*$",
        "riot": r"^riot/.*",
        "zom": r"^zom 2/.*$",
    }
    k = UserAgentParser(apps)
    appname, platform = k.parse_ua(ua_string)
    assert_equal((appname, platform), (expected_appname, expected_platform))

def test_useragent():
    tests = [
        ("Keanu_Example/0.1.0 (iPad; iOS 13.5; Scale/2.00)", "keanu", "ios"),
        ("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36", "other", "windows"),
        ("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Riot/1.6.7 Chrome/80.0.3987.134 Electron/8.0.3 Safari/537.36", "riot-desktop", "windows"),
        ("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0", "other", "windows"),
        ("Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0", "other", "linux"),
        ("Riot/0.11.5 (iPad; iOS 13.5; Scale/2.00)", "riot", "ios"),
        ("Riot/0.11.5 (iPhone; iOS 13.5.1; Scale/3.00)", "riot", "ios"),
        ("Zom 2/2.0.5 (iPad; iOS 13.5; Scale/2.00)", "zom", "ios"),
        ("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36", "other", "linux"),
        ("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Riot/1.6.7 Chrome/80.0.3987.134 Electron/8.0.3 Safari/537.36", "riot-desktop", "linux"),
        ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15", "other", "mac os x"),
        ("Mozilla/5.0 (iPad; CPU OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1", "other", "ios"),
        ("Riot.im/0.9.12 (Linux; U; Android 6.0.1; SM-J500FN Build/MMB29; Flavour GooglePlay; MatrixAndroidSDK 0.9.35", "riot.im", "android"),
    ]
    for test in tests:
        yield check_useragent, test[0], test[1], test[2]
