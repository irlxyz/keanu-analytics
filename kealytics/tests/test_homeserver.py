from nio.events.room_events import MegolmEvent
from nio.events.room_events import RoomMessageText
from nio.events.invite_events import InviteEvent

from nose.tools import assert_equal

from kealytics.homeserver import AdminWhoisResponse
from kealytics.homeserver import ConnectionInfo
from kealytics.homeserver import DeviceInfo
from kealytics.homeserver import SessionInfo
from kealytics.homeserver import event_is_message

from kealytics.homeserver import most_recent_connection

whois_response = AdminWhoisResponse(
    user_id="@test:test.example.com",
    devices={
        '':
        DeviceInfo(sessions=[
            SessionInfo(connections=[
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1593602432955,
                    user_agent='Zom 2/2.0.5 (iPad; iOS 13.5; Scale/2.00)'),
                ConnectionInfo(  # this one is the latest!
                    ip='192.2.0.3',
                    last_seen=1594209737515,
                    user_agent=
                    'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
                ),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1593602428717,
                    user_agent=
                    'Keanu_Example/0.1.0 (iPad; iOS 13.5; Scale/2.00)'),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1594131842846,
                    user_agent='Riot/0.11.5 (iPhone; iOS 13.5.1; Scale/3.00)'),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1591873169827,
                    user_agent=
                    'Mozilla/5.0 (X11; Linux x86_64; rv: 68.0) Gecko/20100101 Firefox/68.0'
                ),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1593551300522,
                    user_agent=
                    'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
                ),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1593097931255,
                    user_agent=
                    'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
                ),
                ConnectionInfo(
                    ip='192.2.0.2',
                    last_seen=1593551420565,
                    user_agent='Riot/0.11.5 (iPhone; iOS 13.5.1; Scale/3.00)')
            ])
        ])
    })


def test_most_recent_connection():
    u = most_recent_connection(whois_response)
    assert_equal(u.last_seen, 1594209737515)
    assert_equal(u.ip, '192.2.0.3')
    assert_equal(
        u.user_agent,
        'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0')


def check_event_is_message(event, is_message):
    assert_equal(event_is_message(event), is_message)


def test_event_is_message():
    tests = [
        (MegolmEvent({
            "event_id": "",
            "sender": "",
            "origin_server_ts": ""
        }, "", "", ""), True),
        (RoomMessageText({
            "event_id": "",
            "sender": "",
            "origin_server_ts": ""
        }, "", "", ""), True),
        (InviteEvent({
            "event_id": "",
            "sender": "",
            "origin_server_ts": ""
        }, ""), False),
    ]
    for test in tests:
        yield check_event_is_message, test[0], test[1]
