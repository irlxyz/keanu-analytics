import unittest

from kealytics.geo import GeoBuckets

class TestGeoBuckets(unittest.TestCase):
    buckets: GeoBuckets

    def setUp(self) -> None:
        config = [
            {"name": "ma", "path": "examples/ma.geojson"},
            {"name": "usa", "path": "examples/usa.geojson"},
        ]
        self.buckets = GeoBuckets(config)

    def test_geo_point_ma(self) -> None:
        # MIT, Massachusetts
        assert self.buckets.bucket(42.360092, -71.094162) == "ma"

    def test_geo_point_usa(self) -> None:
        # White House, Washington
        assert self.buckets.bucket(38.897675, -77.036530) == "usa"

    def test_geo_point_other(self) -> None:
        # Ottawa, Canada
        assert self.buckets.bucket(45.421532, -75.697189) == "other"
