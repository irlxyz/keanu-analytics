Generating Test Data
====================

A simple method to generate test data is by registering and connecting multiple
clients from within the same Firefox browser, using multi-account containers.
The prerequisites for this are:

1. Firefox 57.0 or later with the `Firefox Multi-Account Containers extension
   <https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/>`_
2. Registration enabled on the Synapse server (even if only temporarily)

Firefox setup
-------------

Firefox can be downloaded and installed from `getfirefox.com
<https://getfirefox.com>`_.

On an existing Firefox installation, the `Multi-Account Containers extension
<https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/>`_
must then be installed. The extension allows separating website cookies on a
tab-by-tab basis, ensuring cookies downloaded by one container are not
available to other containers. Local storage used by Riot.im to store encryption
keys is also kept isolated. This allows the easy running of multiple distinct
Matrix clients from within Firefox.

Once the extension is installed, an extension icon will appear in the top right
corner of the browser. Opening this reveals four default containers, named by
default 'Personal', 'Work', 'Shopping' and 'Banking', each with its own
corresponding colour. Clicking on a container opens a new tab within that
container. Tabs can be identified by a coloured stripe underneath the Tab name
and an indicator in browser's main search bar.

Each container can be used to register and use a Matrix client.

Matrix Registration
-------------------

From within a containerized browser tab, visit https://riot.im/app/#/register
to start the registration of a new client.  There are three options available,
'Free', 'Premium' and 'Other'. To use a Matrix test server, 'Other' must be
selected.  This allows the user to type the name of the test server in the
'Homeserver URL' box, before proceeding with the registration. To register an
account, a username, password and an optional recovery passphrase are required.
Once the registration is complete, the user will instantly have access to chat
within the same window.

It is not necessary to setup key backup or to verify the session in order to
produce test data.

The registration step must be completed individually for each client, from
within a separate container. By default Firefox starts with four containers,
but more can be added if more clients are required for testing.

Generating Test Data
--------------------

Each account can now be used to send direct messages, or to send group
messages.
