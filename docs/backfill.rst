Backfill API
============

.. warning::

   The Matrix Application Service API stores a queue of transactions and will
   retry sending those transactions, even across restarts of Synapse, with an
   exponential backoff. Transactions are only considered delivered once the
   Application Service has acknowledged that it was processed without error.
   For this reason, this API is temporary and will be removed once it is no
   longer required for migration.

In order to backfill events into the database, resolving (outdated but
hopefully not too wrong) information from the Homeserver,
:meth:`kealytics.app_service.KeanuAnalytics.handle_message_event` can be called
directly with period, user ID and room ID data.

Example SQL Query
-----------------

You can export a list of events from the Synapse database with the following
query:

.. code-block:: sql

   \copy (SELECT origin_server_ts, sender, room_id
   FROM events
   WHERE (type = 'm.room.member' OR type = 'm.room.encrypted')
     AND sender LIKE '%:neo.keanu.im'
     AND origin_server_ts >= 100
     AND origin_server_ts < 200) TO 'events.csv' CSV;

``neo.keanu.im`` should be replaced with the homeserver domain name to export
only those events that originated on that homeserver.

``100`` should be replaced with the unix timestamp **in milliseconds** of the
start of the period, and ``200`` should be replace with the unix timestamp **in
milliseconds** of the start of the first period that should not be exported.

Example Script
--------------

Assuming a list of events, as tuples of ``(timestamp, sender, room_id)``, a
backfill script will look something like this:

.. code-block:: python

   import csv
   import confuse

   from kealytics.app_service import KeanuAnalytics
   from kealytics.period import period_for_timestamp


   config = confuse.Configuration('KeanuAnalytics', 'kealytics')
   analytics = KeanuAnalytics(config)
   with open("events.csv") as csvfile:
       reader = csv.reader(csvfile)
       for event in reader:
           period = period_for_timestamp(int(event[0]))
           print(period, *event[1:])
           analytics.handle_message_event(period, *event[1:])

Caveats
-------

* Rooms may no longer exist, which means that it will not be possible in those
  cases to determine if a conversation was a private message or a group message.
* All events from a user will be assigned to the latest device used. Arguably,
  this is not a problem if all events are within a period, as in that case only
  the first device used would be recorded and this still gives a single sample
  in that period. If more than one period is covered (likely for this API) then
  this could cause earlier events to be attributed to a client that the user
  had not used until the final period.
